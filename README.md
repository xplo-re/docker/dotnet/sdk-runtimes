# .NET SDK Images with LTS Runtimes

Docker images derived from the official .NET SDK images with current and LTS
runtimes added. Ideal for unit testing with different target frameworks.

Supports multiple Linux platforms.


## Image Contents
Based on the latest official .NET SDK and Runtime images from the
[Microsoft Container Registry (MCR)][mcr].

Takes the official `sdk:x.y-bullseye-slim` SDK image and copies the runtimes
from the latest `aspnet` runtime images for current and LTS releases.

Adds the following command-line utility packages:
- `jq` \
  Lightweight JSON processor.
- `xmlstarlet` \
  Powerful XML toolkit to extract data and programmatically modify XML files.


## Usage
Refer to the following registry when requesting an image:
```
registry.gitlab.com/xplo-re/docker/dotnet/sdk-runtimes
```


## Tags
Images for .NET SDK versions `5.0`, `6.0` and `7.0` are provided:
- **[Full list of all available tags][taglist]**

Tags are created as follows:
- `5.0`, `6.0`, `7.0`, `latest` \
  .NET SDK version (with alias `latest` for the latest .NET SDK version), only
  the major and minor version numbers.
  Images contain all current and LTS runtimes.
- `5.0-5.0`(`-#.#`)\*, `6.0-6.0`(`-#.#`)\*, `7.0-7.0`(`-#.#`)\* \
  .NET SDK version, followed by all additionally included .NET runtime versions
  in descending order (including the runtime version of the SDK). Versions only
  include the major and minor version numbers.
- `#.#.#`(`_`...)?(`-#.#.#`(`_`...)?)\* \
  Full .NET SDK version, followed by the full version numbers of all additionally
  included .NET runtimes in descending order (including any preview/RC version
  components).


## Platforms and OS
The SDK images are based on Debian Linux 11 "Bullseye".

Supported platforms:
- `linux/amd64`
- `linux/arm64/v8`
- `linux/arm/v7`


## Release Schedule
Images are built on a weekly schedule.

[mcr]: https://mcr.microsoft.com/dotnet
[taglist]: https://gitlab.com/xplo-re/docker/dotnet/sdk-runtimes/container_registry