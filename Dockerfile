# Use latest SDK as baseline: Unlike runtime images, SDK images contain several
# environment definitions, additional packages and the PowerShell global tool.
# https://mcr.microsoft.com/v2/dotnet/sdk/tags/list
FROM mcr.microsoft.com/dotnet/sdk:7.0-bullseye-slim AS baseline

# Add only runtimes as otherwise the resulting image grows too large with
# additional SDKs. Use ASP.NET runtime images as SDK images are based on these.
# This should include all LTS and currently supported non-LTS runtimes.
# https://mcr.microsoft.com/v2/dotnet/aspnet/tags/list
FROM mcr.microsoft.com/dotnet/aspnet:2.1-stretch-slim  AS dotnet-2.1
FROM mcr.microsoft.com/dotnet/aspnet:3.1-bullseye-slim AS dotnet-3.1
FROM mcr.microsoft.com/dotnet/aspnet:5.0-bullseye-slim AS dotnet-5.0
FROM mcr.microsoft.com/dotnet/aspnet:6.0-bullseye-slim AS dotnet-6.0

# Copy host/fxr/ and runtimes from shared/ directory; each contain per-version
# subdirectories and will not overwrite other files. Filtering via .dockerignore
# is not an option as it is not supported in combination with --from.
FROM baseline

COPY --from=dotnet-2.1 ["/usr/share/dotnet/host",   "/usr/share/dotnet/host/"]
COPY --from=dotnet-2.1 ["/usr/share/dotnet/shared", "/usr/share/dotnet/shared/"]

COPY --from=dotnet-3.1 ["/usr/share/dotnet/host",   "/usr/share/dotnet/host/"]
COPY --from=dotnet-3.1 ["/usr/share/dotnet/shared", "/usr/share/dotnet/shared/"]

COPY --from=dotnet-5.0 ["/usr/share/dotnet/host",   "/usr/share/dotnet/host/"]
COPY --from=dotnet-5.0 ["/usr/share/dotnet/shared", "/usr/share/dotnet/shared/"]

COPY --from=dotnet-6.0 ["/usr/share/dotnet/host",   "/usr/share/dotnet/host/"]
COPY --from=dotnet-6.0 ["/usr/share/dotnet/shared", "/usr/share/dotnet/shared/"]

# Install additional command-line utilities.
RUN set -eux; \
    uname -a; \
    apt-get update; \
    apt-get install -y --no-install-recommends \
        # Lightweight JSON processor.
        jq \
        # XML toolkit.
        xmlstarlet \
    ; \
    rm -rf /var/lib/apt/lists/*